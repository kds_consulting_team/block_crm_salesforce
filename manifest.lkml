constant: connection {
  value: "keboola_block_crm_salesforce"
}

# url of your Salesforce domain for object links
constant: domain {
  value: "keboola.lightning.force.com"
}
